// /* global cv */

import { diff, norm } from '../utils';

// Port of https://github.com/opencv/opencv/blob/a50a355/modules/features2d/src/blobdetector.cpp
// But with special `faster` option which has slightly different semantics,
// but is a whole bunch faster.

const defaultParams = {
  thresholdStep: 10,
  minThreshold: 50,
  maxThreshold: 220,
  minRepeatability: 2,
  minDistBetweenBlobs: 10,

  filterByColor: true,
  blobColor: 0,

  filterByArea: true,
  minArea: 25,
  maxArea: 5000,

  filterByCircularity: false,
  minCircularity: 0.8,
  maxCircularity: 1000000,

  filterByInertia: true,
  //minInertiaRatio: 0.6,
  minInertiaRatio: 0.1,
  maxInertiaRatio: 1000000,

  filterByConvexity: true,
  //minConvexity: 0.8,
  minConvexity: 0.95,
  maxConvexity: 1000000,

  faster: false,
  scaleFactor: 4,
};

function findBlobs(boxes) {
  // const contours = new cv.MatVector();
  // const hierarchy = new cv.Mat();
  // if (params.faster) {
  //   cv.findContours(binaryImage, contours, hierarchy, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE);
  // } else {
  //   cv.findContours(binaryImage, contours, hierarchy, cv.RETR_LIST, cv.CHAIN_APPROX_NONE);
  // }
  // hierarchy.delete();

  const centers = [];
  const objectsToDelete = [];
  for (let i = 0; i < boxes.length; i++) {
    const box = boxes[i];
    objectsToDelete.push(box);
    

    const x = box[0];
    const y = box[1];
    const width = box[2];
    const height = box[3];
    const center = {
      confidence: 1,
      location: { x: x + width / 2, y: y + height / 2 },
      radius: (width + height) / 4,
    };

    centers.push(center);
  }
  objectsToDelete.forEach(obj => obj.delete());
  boxes.delete();
  return centers;
}

export default function simpleBlobDetector(image, boxes, params) {
  params = { ...defaultParams, ...params };

  params.minArea /= params.scaleFactor;
  params.maxArea /= params.scaleFactor;

  // const scaledSize = new cv.Size(image.cols / params.scaleFactor, image.rows / params.scaleFactor);
  // const scaledImage = new cv.Mat(scaledSize, image.type());
  // cv.resize(image, scaledImage, scaledSize, 0, 0, cv.INTER_LINEAR);

  // const grayScaleImage = new cv.Mat(scaledSize, cv.CV_8UC1);
  // cv.cvtColor(scaledImage, grayScaleImage, cv.COLOR_RGB2GRAY);

  // scaledImage.delete();

  let centers = [];
  for (
    let thresh = params.minThreshold;
    thresh < params.maxThreshold;
    thresh += params.thresholdStep
  ) {
    // const binaryImage = new cv.Mat(scaledSize, cv.CV_8UC1);
    // cv.threshold(grayScaleImage, binaryImage, thresh, 255, cv.THRESH_BINARY);
    let curCenters = findBlobs(boxes);
    // binaryImage.delete();

    let newCenters = [];
    for (let i = 0; i < curCenters.length; i++) {
      curCenters[i].location.x *= params.scaleFactor;
      curCenters[i].location.y *= params.scaleFactor;
      curCenters[i].radius *= params.scaleFactor;

      let isNew = true;
      for (let j = 0; j < centers.length; j++) {
        const dist = norm(
          diff(centers[j][Math.floor(centers[j].length / 2)].location, curCenters[i].location)
        );
        isNew =
          dist >= params.minDistBetweenBlobs &&
          dist >= centers[j][Math.floor(centers[j].length / 2)].radius &&
          dist >= curCenters[i].radius;
        if (!isNew) {
          centers[j].push(curCenters[i]);

          let k = centers[j].length - 1;
          while (k > 0 && centers[j][k].radius < centers[j][k - 1].radius) {
            centers[j][k] = centers[j][k - 1];
            k--;
          }
          centers[j][k] = curCenters[i];
          break;
        }
      }
      if (isNew) newCenters.push([curCenters[i]]);
    }
    centers = centers.concat(newCenters);
  }

  const keyPoints = [];
  for (let i = 0; i < centers.length; i++) {
    if (centers[i].length < params.minRepeatability) continue;
    const sumPoint = { x: 0, y: 0 };
    let normalizer = 0;
    for (let j = 0; j < centers[i].length; j++) {
      sumPoint.x += centers[i][j].confidence * centers[i][j].location.x;
      sumPoint.y += centers[i][j].confidence * centers[i][j].location.y;
      normalizer += centers[i][j].confidence;
    }
    sumPoint.x *= 1 / normalizer;
    sumPoint.y *= 1 / normalizer;
    let size = Math.round(centers[i][Math.floor(centers[i].length / 2)].radius * 2);
    size = Math.min(
      size,
      sumPoint.x * 2,
      sumPoint.y * 2,
      (image.cols - sumPoint.x) * 2,
      (image.rows - sumPoint.y) * 2
    );
    keyPoints.push({ pt: sumPoint, size });
  }

  return keyPoints;
}
