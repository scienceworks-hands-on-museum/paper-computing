const Block = `// Block

importScripts('paper.js');

(async () => {
  await paper.set('data', { start: false });
  await paper.set('data', { end: false });
  await paper.set('data', { block: true });
  await paper.set('data', { blockPoints: [] });
  await paper.set('data', { sync: false });
})();
`;

module.exports = Block;
