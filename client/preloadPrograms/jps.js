const jps = `// jps

importScripts('paper.js');

(async () => {
  await paper.set('data', { jps: true });
})();
`;

module.exports = jps;
