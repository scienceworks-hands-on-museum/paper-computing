const Dijkstra = `// Dijkstra

importScripts('paper.js');

(async () => {
  await paper.set('data', { djikstra: true });
})();
`;

module.exports = Dijkstra;
