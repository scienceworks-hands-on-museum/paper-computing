const Finish = `// Finish

importScripts('paper.js');

(async () => {
  await paper.set('data', { start: false });
  await paper.set('data', { end: true });
  await paper.set('data', { block: false });
})();
`;

module.exports = Finish;
