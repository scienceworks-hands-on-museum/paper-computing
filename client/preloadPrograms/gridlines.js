const gridlines = `// gridlines

importScripts('paper.js');

startColor = "green";
endColor = "red";

djikColorSeen = "rgb(50, 30, 0)";
aStarColorSeen = "rgb(0, 20, 40)";
jpsColorSeen = "rgb(0, 0, 75)";

djikColorPath = "rgb(150, 100, 51)";
aStarColorPath = "rgb(50, 50, 125)";
jpsColorPath = "rgb(50, 50, 150)";
jpsColorPath = "rgb(50, 50, 150)";

global_sync = true;
global_unreachable = false;
global_blockpoints = [];

class Node {
  constructor(x, y, blocked) {
    this.x = x;
    this.y = y;
    this.blocked = blocked;
    this.edges = new Map();
    this.djikVisited = false;
    this.aStarVisited = false;
    this.jpsVisited = false;
    this.jpsVisited = false;
    this.djikPath = false;
    this.aStarPath = false;
    this.jpsPath = false;
    this.jpsPath = false;
    this.start = false;
    this.end = false;
    this.paperStart = false;
    this.paperEnd = false;
  }

  connect(node, weight = 1) {
    this.edges.set(node, weight);
    node.edges.set(this, weight);
  }

  disconnect(node) {
    this.edges.delete(node);
    node.edges.delete(this);
  }

  // Color based on algorithm
  color(supporterCtx, gridWidth) {
    if (this.start) {
      fillBoxOneColor(this.x, this.y, startColor, supporterCtx, gridWidth);
      supporterCtx.globalCompositeOperation = "source-over";
      supporterCtx.font = '32px sans-serif';
      supporterCtx.textAlign = 'center';
      supporterCtx.fillStyle = 'rgb(0,255,0)';
      if (!this.paperStart) {
        supporterCtx.fillText("Start", this.x * gridWidth + 0.5 * gridWidth, this.y * gridWidth - 0.2 * gridWidth);
      }
    }
    else if (this.end) {
      fillBoxOneColor(this.x, this.y, endColor, supporterCtx, gridWidth);

      supporterCtx.globalCompositeOperation = "source-over";
      supporterCtx.font = '32px sans-serif';
      supporterCtx.textAlign = 'center';
      supporterCtx.fillStyle = 'rgb(255,0,0)';
      if (!this.paperEnd) {
        supporterCtx.fillText('Finish', this.x * gridWidth + 0.5 * gridWidth, this.y * gridWidth - 0.2 * gridWidth);
      }
    } 
    else if (this.djikPath && this.jps) {
      fillBoxTwoColor(this.x, this.y, djikColorPath, jpsColorPath, supporterCtx, gridWidth);
    }
    else if (this.jps && this.aStarPath) {
      fillBoxTwoColor(this.x, this.y, jpsColorPath, aStarColorPath, supporterCtx, gridWidth);
    }
    else if (this.djikPath && this.aStarPath) {
      fillBoxTwoColor(this.x, this.y, djikColorPath, aStarColorPath, supporterCtx, gridWidth);
    }
    else if (this.djikPath) {
      fillBoxOneColor(this.x, this.y, djikColorPath, supporterCtx, gridWidth);
    }
    else if (this.aStarPath) {
      fillBoxOneColor(this.x, this.y, aStarColorPath, supporterCtx, gridWidth);
    }
    else if (this.jpsPath) {
      fillBoxOneColor(this.x, this.y, jpsColorPath, supporterCtx, gridWidth);
    }
    else if (this.djikVisited && this.aStarVisited) {
      fillBoxTwoColor(this.x, this.y, djikColorSeen, aStarColorSeen, supporterCtx, gridWidth);
    }
    else if (this.jpsVisited && this.jpsVisited) {
      fillBoxTwoColor(this.x, this.y, jpsColorSeen, jpsColorSeen, supporterCtx, gridWidth);
    }
    else if (this.jpsPath) {
      fillBoxOneColor(this.x, this.y, jpsColorPath, supporterCtx, gridWidth);
    }
    else if (this.djikVisited && this.aStarVisited) {
      fillBoxTwoColor(this.x, this.y, djikColorSeen, aStarColorSeen, supporterCtx, gridWidth);
    }
    else if (this.jpsVisited && this.jpsVisited) {
      fillBoxTwoColor(this.x, this.y, jpsColorSeen, jpsColorSeen, supporterCtx, gridWidth);
    }
    else if (this.djikVisited && this.aStarVisited) {
      fillBoxTwoColor(this.x, this.y, djikColorSeen, aStarColorSeen, supporterCtx, gridWidth);
    }
    else if (this.djikVisited) {
      fillBoxOneColor(this.x, this.y, djikColorSeen, supporterCtx, gridWidth);
    }
    else if (this.aStarVisited) {
      fillBoxOneColor(this.x, this.y, aStarColorSeen, supporterCtx, gridWidth);
    }
    else if (this.jpsVisited) {
      fillBoxOneColor(this.x, this.y, jpsColorSeen, supporterCtx, gridWidth);
    }
    else if (this.jpsVisited) {
      fillBoxOneColor(this.x, this.y, jpsColorSeen, supporterCtx, gridWidth);
    }
  }
}

async function fillBoxOneColor(x, y, color, supporterCtx, gridWidth) {
  supporterCtx.globalCompositeOperation = "destination-over";
  supporterCtx.fillStyle = color;
  supporterCtx.fillRect(x * gridWidth, y * gridWidth, gridWidth, gridWidth);
  supporterCtx.stroke();
}

async function fillBoxTwoColor(x, y, color1, color2, supporterCtx, gridWidth) {
  supporterCtx.globalCompositeOperation = "destination-over";
  supporterCtx.fillStyle = color1;
  supporterCtx.beginPath();
  supporterCtx.moveTo(x * gridWidth, y * gridWidth);
  supporterCtx.lineTo((x + 1) * gridWidth, y * gridWidth);
  supporterCtx.lineTo(x * gridWidth, (y + 1) * gridWidth);
  supporterCtx.closePath();
  supporterCtx.fill();
  supporterCtx.stroke();

  supporterCtx.fillStyle = color2;
  supporterCtx.beginPath();
  supporterCtx.moveTo((x + 1) * gridWidth, (y + 1) * gridWidth);
  supporterCtx.lineTo((x + 1) * gridWidth, y * gridWidth);
  supporterCtx.lineTo(x * gridWidth, (y + 1) * gridWidth);
  supporterCtx.closePath();
  supporterCtx.fill();
  supporterCtx.stroke();
}

async function createMaze(width, height, gridWidth) {
  const papers = await paper.get('papers');
  let blockNums = [];
  let blockCenters = [];
  for (i = 10; i <= 20; i++) {
    try {
      if (papers[i].data.block) {
        blockNums.push(i);
        blockCenters.push([(papers[i].points.center.x / gridWidth).toFixed(0), (papers[i].points.center.y / gridWidth).toFixed(0)]);
      }
    } catch (e){}
  }

  const maze = [];


  for (let x = 0; x < width; x++) {
    const row = [];
    for (let y = 0; y < height; y++) {
      let initLen = row.length;
      for (let block = 0; block < blockNums.length; block++) {
        // console.log(blockNums[block]);
        if (await pointInBlock(papers[blockNums[block]].points, x * gridWidth, y * gridWidth)) {  
          row.push(new Node(x, y, true));
          if (y > 0) {
            row[y - 1].blocked = true;
            if (x > 0) {
              maze[x - 1][y - 1].blocked = true;
              maze[x - 1][y].blocked = true;
            }
          }
          block = blockNums.length;
        }
      }
      if (initLen == row.length) {
        row.push(new Node(x, y, false));
      }
    }
    maze.push(row);
  }

  // Connect nodes horizontally and vertically
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      if (!maze[x][y].blocked) {
        if (x > 0 && !maze[x - 1][y].blocked) {
          maze[x][y].connect(maze[x - 1][y]);
        }
        if (y > 0 && !maze[x][y - 1].blocked) {
          maze[x][y].connect(maze[x][y - 1]);
        }
      }      
    }
  }
  return Promise.resolve(maze);
}

async function drawGridlines(supporterCtx, maze, gridWidth) {
  supporterCtx.globalCompositeOperation = "destination-over";
  supporterCtx.strokeStyle = 'rgb(25,25,25)';
  supporterCtx.lineWidth = 1;
  for (let x = 0; x < maze.length; x++) {
    for (let y = 0; y < maze[0].length; y++) {
      if (!maze[x][y].blocked) {
        supporterCtx.strokeRect(x * gridWidth, y * gridWidth, gridWidth, gridWidth);
      }
    }
  }
}

async function drawBoxWords(supporterCtx, gridWidth) {
  // let papers = await paper.get('papers');
  // let myPaperNumber = await paper.get('number');

  // for (otherPaperNumber of Object.keys(papers)) {
  //   if (otherPaperNumber !== myPaperNumber) {
  //     if (papers[otherPaperNumber].data.block == true) {
  //       let center = papers[otherPaperNumber].points.center;
  //       supporterCtx.globalCompositeOperation = "source-over";
  //       supporterCtx.font = '20px sans-serif';
  //       supporterCtx.textAlign = 'center';
  //       supporterCtx.fillStyle = 'rgb(255,0,0)';
  //       supporterCtx.fillStyle = 'rgb(255,255,0)';
  //       supporterCtx.fillText('Block', center.x, center.y - 10);
  //     }
  //   }
  // }
}

async function drawUnreachable(supporterCtx, supporterCanvas) {
  if (global_unreachable) {
    supporterCtx.globalCompositeOperation = "source-over";
    supporterCtx.font = '64px sans-serif';
    supporterCtx.textAlign = 'center';
    supporterCtx.fillStyle = 'rgb(255,0,0)';
    supporterCtx.fillText('You stumped the Algorithm!', supporterCanvas.width / 2, supporterCanvas.height / 2 - 30);
    supporterCtx.fillText('ENDPOINT UNREACHABLE', supporterCanvas.width / 2, supporterCanvas.height / 2 + 30);
  }
}

async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function drawAll(maze, supporterCtx, supporterCanvas, gridWidth, startX, startY, endX, endY) {
  drawGridlines(supporterCtx, maze, gridWidth);
  drawUnreachable(supporterCtx, supporterCanvas);
  // for (let i = 0; i < global_blockpoints.length; i++) {
  //   supporterCtx.globalCompositeOperation = "source-over";
  //   supporterCtx.font = '32px sans-serif';
  //   supporterCtx.textAlign = 'center';
  //   supporterCtx.fillStyle = 'rgb(255,255,0)';
  //   supporterCtx.fillText('Block', global_blockpoints[i].x,global_blockpoints[i].y);
  // }
  for (let x = 0; x < maze.length; x++) {
    for (let y = 0; y < maze[0].length; y++) {
      maze[x][y].color(supporterCtx, gridWidth);
    }
  }
  return maze;
}

function triangleArea(x1, y1, x2, y2, x3, y3) {
  let area = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2);
  return area;
}

async function scalePolygon(vertices, scaleFactor, center) {
  // Translate the polygon so that the center point coincides with the origin
  const translatedVertices = vertices.map(vertex => ({
    x: vertex.x - center.x,
    y: vertex.y - center.y
  }));

  // Scale the translated polygon by the given factor
  const scaledVertices = translatedVertices.map(vertex => ({
    x: vertex.x * scaleFactor,
    y: vertex.y * scaleFactor
  }));

  // Translate the scaled polygon back to its original position
  const finalVertices = scaledVertices.map(vertex => ({
    x: vertex.x + center.x,
    y: vertex.y + center.y
  }));

  return finalVertices;
}

async function pointInBlock(blPoints, x, y) {
  let roundNum = 8;

  let vertices = [blPoints.topLeft, blPoints.topRight, blPoints.bottomRight, blPoints.bottomLeft];
  // let scaledVer = await scalePolygon(vertices, 1.36, blPoints.center);
  let scaledVer = await scalePolygon(vertices, 1.3345, blPoints.center);

  let tl_x = scaledVer[0].x;
  let tl_y = scaledVer[0].y;
  let tr_x = scaledVer[1].x;
  let tr_y = scaledVer[1].y;
  let br_x = scaledVer[2].x;
  let br_y = scaledVer[2].y;
  let bl_x = scaledVer[3].x;
  let bl_y = scaledVer[3].y;
  let x_rnd = x.toFixed(roundNum);
  let y_rnd = y.toFixed(roundNum);
  let sumTriangleArea = (triangleArea(x, y, tl_x, tl_y, tr_x, tr_y) +
                        triangleArea(x, y, tl_x, tl_y, bl_x, bl_y) +
                        triangleArea(x, y, br_x, br_y, tr_x, tr_y) +
                        triangleArea(x, y, br_x, br_y, bl_x, bl_y)).toFixed(roundNum);
  let rectArea = (triangleArea(bl_x, bl_y, tl_x, tl_y, tr_x, tr_y) + triangleArea(bl_x, bl_y, br_x, br_y, tr_x, tr_y)).toFixed(roundNum);                           
  return sumTriangleArea == rectArea || sumTriangleArea == 0;
}


//////////////////////
// Dijkstras start 
//////////////////////

async function dijkstra(maze, supporterCtx, gridWidth, startX, startY, endX, endY) {
  const startNode = maze[startX][startY];
  const endNode = maze[endX][endY];
  const distance = new Map();
  const previous = new Map();
  const unvisited = new Set();
  const speed = 1;

  // Initialize distances and previous nodes
  for (let row of maze) {
    for (let node of row) {
      distance.set(node, Infinity);
      previous.set(node, null);
      unvisited.add(node);
    }
  }
  distance.set(startNode, 0);


  while (unvisited.size > 0) {
    if (global_sync) {
      return;
    }

    let current = null;
    for (let node of unvisited) {
      if (current === null || distance.get(node) < distance.get(current)) {
        current = node;        
      }
    }
    if (distance.get(current) == Infinity) {
      break;
    }

    // console.log('dist:' + distance.get(current));

    // Exit loop if end node is reached
    if (current === endNode) {
      break;
    }

    // Mark current node as visited
    current.djikVisited = true;
    unvisited.delete(current);

    // Update distances for neighboring nodes
    for (let [neighbor, weight] of current.edges) {
      if (!neighbor.djikVisited) {
        const alt = distance.get(current) + weight;
        if (alt < distance.get(neighbor)) {
          distance.set(neighbor, alt);
          previous.set(neighbor, current);
        }
      }
    }
    // console.log("a: " + time.getTime())
    await sleep(speed);
    // console.log("b: " + time.getTime());
  }

  // Construct path from start to end
  const path = [];
  let current = endNode;
  if (previous.get(current) === null) {
    global_unreachable = true;
    return path
  }

  while (current !== null) {
    path.unshift({ x: current.x, y: current.y });
    current.djikPath = true;
    current = previous.get(current);
  }
  return path;
}


//////////////////////
// ASTAR start 
//////////////////////

async function aStarPathfinding(maze, supporterCtx, gridWidth, startX, startY, endX, endY) {
  const startNode = maze[startX][startY];
  const endNode = maze[endX][endY];
  const openSet = new Set();
  const closedSet = new Set();
  const previous = new Map();
  const gScore = new Map();
  const distanceFScore = new Map();
  const speed = 1;

  // Initialize scores
  for (let row of maze) {
    for (let node of row) {
      gScore.set(node, Infinity);
      distanceFScore.set(node, Infinity);
      previous.set(node, null);
    }
  }
  gScore.set(startNode, 0);
  distanceFScore.set(startNode, heuristic(startNode, endNode));

  openSet.add(startNode);

  while (openSet.size > 0) {
    if (global_sync) {
      return;
    }

    let current = null;
    for (let node of openSet) {
      if (current === null || distanceFScore.get(node) < distanceFScore.get(current)) {
        current = node;
      }
    }

    if (distanceFScore.get(current) == Infinity) {
      global_unreachable = true;
      break;
    }
    // break if the end point is found
    if (current === endNode) {
      break;
    }

    openSet.delete(current);
    closedSet.add(current);
    current.aStarVisited = true;

    for (let [neighbor, weight] of current.edges) {
      if (closedSet.has(neighbor)) {
        continue;
      }

      const tentativeGScore = gScore.get(current) + weight;
      if (!openSet.has(neighbor)) {
        openSet.add(neighbor);
      } else if (tentativeGScore >= gScore.get(neighbor)) {
        continue;
      }

      previous.set(neighbor, current);
      gScore.set(neighbor, tentativeGScore);
      distanceFScore.set(neighbor, tentativeGScore + heuristic(neighbor, endNode));
    }

    await sleep(speed);
  }

  // make path and return
  const path = [];
  let current = endNode;
  if (!previous.has(current)) {
    global_unreachable = true;
    return path;
  }

  while (current !== null) {
    path.unshift({ x: current.x, y: current.y });
    current.aStarPath = true;
    current = previous.get(current);
  }

  return path;
}

function heuristic(nodeA, nodeB) {
  // Use the Manhattan distance as the heuristic function
  return Math.abs(nodeA.x - nodeB.x) + Math.abs(nodeA.y - nodeB.y);
}


//////////////////////
// JPS start 
//////////////////////

async function JPS(maze, supporterCtx, gridWidth, startX, startY, endX, endY) {
  const startNode = maze[startX][startY];
  const endNode = maze[endX][endY];
  const openSet = new Set();
  const closedSet = new Set();
  const previous = new Map();
  const gScore = new Map();
  const fScore = new Map();
  const speed = 1;

  // Initialize gScores and fScores
  for (let row of maze) {
    for (let node of row) {
      gScore.set(node, Infinity);
      fScore.set(node, Infinity);
    }
  }
  gScore.set(startNode, 0);
  fScore.set(startNode, heuristic(startNode, endNode));

  openSet.add(startNode);

  while (openSet.size > 0) {
    if (global_sync) {
      return;
    }

    let current = null;
    for (let node of openSet) {
      if (current === null || fScore.get(node) < fScore.get(current)) {
        current = node;
      }
    }

    if (current === endNode) {
      await reconstructPath(previous, current, supporterCtx, gridWidth);
      return;
    }

    openSet.delete(current);
    closedSet.add(current);

    const neighbors = jumpPointNeighbors(current, maze, startX, startY, endX, endY);
    for (let neighbor of neighbors) {
      if (closedSet.has(neighbor)) {
        continue;
      }

      const tentativeGScore = gScore.get(current) + distance(current, neighbor);

      if (!openSet.has(neighbor)) {
        openSet.add(neighbor);
      } else if (tentativeGScore >= gScore.get(neighbor)) {
        continue;
      }

      previous.set(neighbor, current);
      gScore.set(neighbor, tentativeGScore);
      fScore.set(neighbor, gScore.get(neighbor) + heuristic(neighbor, endNode));
    }

    await sleep(speed);

  }

  global_unreachable = true;
}


//////////////////////
// async func
//////////////////////


(async () => {
  let supporterCanvas = await paper.get('supporterCanvas', {id: 1});
  let supporterCtx = supporterCanvas.getContext('2d');

  let supporterCanvas2 = await paper.get('supporterCanvas', {id: 2});
  let supporterCtx2 = supporterCanvas2.getContext('2d');

  const myPaperNumber = await paper.get('number');
  await paper.set('data', { start: false });
  await paper.set('data', { end: false });
  await paper.set('data', { block: false });
  await paper.set('data', { blockPoints: [] });
  await paper.set('data', { sync: false }); 

  const width = 40;
  
  const gridWidth = supporterCanvas.width / width;
  const height = Math.floor(supporterCanvas.height / gridWidth);
  let maze = await createMaze(width, height, gridWidth);

  var startX = 2;
  var startY = 2;
  var endX = 5;
  var endY = 5;

  var prevStartX = -1;
  var prevStartY = -1;
  var prevEndX = -1;
  var prevEndY = -1;

  const startNum = 2;
  const endNum = 3;

  const djikNum = 1500;
  const starNum = 1600;
  const jpsNum = 1700;

  var current_algs = [];
  var paperStart = false;
  var paperEnd = false;

  setInterval(async () => {
    const papers = await paper.get('papers');
    const myPaperNumber = await paper.get('number');

    try {
      startX = (papers[startNum].points.center.x / gridWidth).toFixed(0);
      startY = (papers[startNum].points.center.y / gridWidth).toFixed(0);
      paperStart = true;
    } catch (e) {
      startX = 2;
      startY = 2;
      paperStart = false;
    }

    try {
      endX = (papers[endNum].points.center.x / gridWidth).toFixed(0);
      endY = (papers[endNum].points.center.y / gridWidth).toFixed(0);
      paperEnd = true;
    } catch (e) {
      endX = width - 3;
      endY = height - 3;
      paperEnd = false;
    }

    // console.log(maze);

    maze[startX][startY].start = true;
    maze[startX][startY].paperStart = paperStart;
    maze[endX][endY].end = true;
    maze[endX][endY].paperEnd = paperEnd;


    let blockPoints = [];
    let algs = [];    
    for (i = 10; i <= 20; i++) {
      try {
        if (papers[i].data.block) {
          blockPoints.push(papers[i].points.center);
        }
      } catch (e){}
    }
    try {
      if (papers[djikNum].data.djikstra) {
        algs.push(djikNum);
      }
    } catch (e) {}
    try {
      if (papers[starNum].data.astar){
        algs.push(starNum)
      }
    } catch (e) {}


    if (blockPoints.length == global_blockpoints.length
        && algs.length == current_algs.length
        && prevStartX == startX && prevStartY == startY
        && prevEndX == endX && prevEndY == endY) {
      for (let i = 0; i < blockPoints.length; i++) {
        if (Math.abs(blockPoints[i].x - global_blockpoints[i].x) > .028125 * supporterCanvas.width 
        || Math.abs(blockPoints[i].y - global_blockpoints[i].y) > .05 * supporterCanvas.height || (i < algs.length && algs[i] != current_algs[i]) ) {

          global_blockpoints = blockPoints;
          global_unreachable = false;
          global_sync = true;
          
          current_algs = algs;
          prevStartX = startX;
          prevStartY = startY;
          prevEndX = endX;
          prevEndY = endY;

          maze = await createMaze(width, height, gridWidth);
          
          
          maze[startX][startY].start = true;
          maze[startX][startY].paperStart = paperStart;
          maze[endX][endY].end = true;
          maze[endX][endY].paperEnd = paperEnd;

          global_sync = false;
          try {
            if (papers[djikNum].data.djikstra) {
              let path = dijkstra(maze, supporterCtx, gridWidth, startX, startY, endX, endY);
            } }
            catch (e) {}
          try { if (papers[starNum].data.astar) {
              let path = aStarPathfinding(maze, supporterCtx, gridWidth, startX, startY, endX, endY);
            }
          } catch (e) {}
          try { if (papers[jpsNum].data.jps) {
              let path = JPS(maze, supporterCtx, gridWidth, startX, startY, endX, endY);
            }
          } catch (e) {}
        }
      }
    } else {
      global_blockpoints = blockPoints;
      global_unreachable = false;
      global_sync = true;

      current_algs = algs;
      prevStartX = startX;
      prevStartY = startY;
      prevEndX = endX;
      prevEndY = endY;
      
      maze = await createMaze(width, height, gridWidth);

      maze[startX][startY].start = true;
      maze[startX][startY].paperStart = paperStart;
      maze[endX][endY].end = true;
      maze[endX][endY].paperEnd = paperEnd;

      global_sync = false;
      try {
        if (papers[djikNum].data.djikstra) {
          let path = dijkstra(maze, supporterCtx, gridWidth, startX, startY, endX, endY);
        } 
      } catch (e) {}
      try {
        if (papers[starNum].data.astar) {
          let path = aStarPathfinding(maze, supporterCtx, gridWidth, startX, startY, endX, endY);
        }
      } catch (e) {}
      try {
        if (papers[jpsNum].data.jps) {
          let path = JPS(maze, supporterCtx, gridWidth, startX, startY, endX, endY);
        }
      } catch (e) {}
    }


    // for (i = 0; i < 10; i++) {
    //   maze[10 + i][10].djikVisited = true;
    // }
    // for (i = 0; i < 10; i++) {
    //   maze[10 + i][11].djikPath = true;
    // }

    // for (i = 0; i < 10; i++) {
    //   maze[10 + i][13].aStarVisited = true;
    // }
    // for (i = 0; i < 10; i++) {
    //   maze[10 + i][14].aStarPath = true;
    // }

    // for (i = 0; i < 10; i++) {
    //   maze[10 + i][16].djikVisited = true;
    //   maze[10 + i][16].aStarVisited = true;
    // }
    // for (i = 0; i < 10; i++) {
    //   maze[10 + i][17].djikPath = true;
    //   maze[10 + i][17].aStarPath = true;
    // }

    supporterCtx.clearRect(0, 0, supporterCanvas.width, supporterCanvas.height);
    await drawAll(maze, supporterCtx, supporterCanvas, gridWidth, startX, startY, endX, endY);
    // supporterCtx.clearRect(0, 0, supporterCanvas.width, supporterCanvas.height);
  }, 50);
})();
`;

module.exports = gridlines;
