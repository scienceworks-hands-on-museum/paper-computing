const Start = `// Start

importScripts('paper.js');

(async () => {
  await paper.set('data', { start: true });
  await paper.set('data', { end: false });
  await paper.set('data', { block: false });
})();
`;

module.exports = Start;
