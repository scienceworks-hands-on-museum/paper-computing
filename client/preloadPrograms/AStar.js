const AStar = `// AStar

importScripts('paper.js');

(async () => {
  await paper.set('data', { astar: true });
})();
`;

module.exports = AStar;
