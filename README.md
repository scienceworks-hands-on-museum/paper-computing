# Paper Programs

**Create Interactive Games with Paper!**

**For Local Installation**

## Running in codespaces

```
sudo su - postgres
psql -c "CREATE ROLE codepaper superuser createdb login;"
exit
nvm install
npm install
npm run dev
```

