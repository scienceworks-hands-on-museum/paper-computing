from fastapi import FastAPI
import asyncio
from fastapi import WebSocket

app = FastAPI()

from ultralytics import YOLO

class BackgroundRunner:
    def __init__(self):
        self.result = None

    async def run_main(self):
        while True:
            results = model.predict(source=4, stream=True)
            for r in results:
                self.result = r.boxes.xywh
                await asyncio.sleep(0.02)

runner = BackgroundRunner()
model = YOLO("/home/paper/CodePaper/yolo/best_deterTrained.pt")

@app.on_event('startup')
async def app_startup():
    asyncio.create_task(runner.run_main())

@app.get("/")
async def root():
    return str(runner.result)

@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    while True:
        await asyncio.sleep(0.02)
        await websocket.send_json(runner.result.cpu().numpy().tolist())
