from ultralytics import YOLO

# Load a model
# model = YOLO("yolov8n.pt")  # load an official model
model = YOLO("best.pt")  # load a custom model
# Predict with the model

results = model.train(data='/home/paper/CodePaper/yoloTrainer/coco1000.yaml', batch=8,device=0, epochs=1800, imgsz=[640, 360])
